/**
 * 
 */
package TresEnRaya.TresEnRaya;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridLayout;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import java.awt.CardLayout;
import java.awt.GridBagLayout;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;

/**
 * @author Nayara Nivea Gomes Santos
 *
 */
public class Vista extends JFrame {
	
	private JPanel contentPane;
	private JButton btn1;
	private JButton btn2;
	private JButton btn3;
	private JButton btn4;
	private JButton btn5;
	private JButton btn6;
	private JButton btn7;
	private JButton btn8;
	private JButton btn9;
	
	private final int JUGADOR_UNO = 1;
	private final int JUGADOR_DOS = 2;
	
	private int jugador = 0;
	private int[] xo = new int[9];
	
	public Vista() {
		setTitle("Tres en Raya");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 655, 530);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		btn1 = new JButton("");
		btn1.setFont(new Font("Tahoma", Font.PLAIN, 90));
		
		btn2 = new JButton("");
		btn2.setFont(new Font("Tahoma", Font.PLAIN, 90));
		
		btn3 = new JButton("");
		btn3.setFont(new Font("Tahoma", Font.PLAIN, 90));
		
		btn7 = new JButton("");
		btn7.setFont(new Font("Tahoma", Font.PLAIN, 90));
		
		btn8 = new JButton("");
		btn8.setFont(new Font("Tahoma", Font.PLAIN, 90));
		
		btn9 = new JButton("");
		btn9.setFont(new Font("Tahoma", Font.PLAIN, 90));
		
		btn4 = new JButton("");
		btn4.setFont(new Font("Tahoma", Font.PLAIN, 90));
		
		btn5 = new JButton("");
		btn5.setFont(new Font("Tahoma", Font.PLAIN, 90));
		
		btn6 = new JButton("");
		btn6.setFont(new Font("Tahoma", Font.PLAIN, 90));
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(78)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
						.addComponent(btn4, GroupLayout.PREFERRED_SIZE, 143, GroupLayout.PREFERRED_SIZE)
						.addComponent(btn1, GroupLayout.PREFERRED_SIZE, 143, GroupLayout.PREFERRED_SIZE)
						.addComponent(btn7, GroupLayout.PREFERRED_SIZE, 143, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(btn8, GroupLayout.PREFERRED_SIZE, 143, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(btn9, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
								.addComponent(btn5, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(btn2, Alignment.TRAILING, GroupLayout.PREFERRED_SIZE, 143, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING, false)
								.addComponent(btn6, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(btn3, GroupLayout.PREFERRED_SIZE, 143, GroupLayout.PREFERRED_SIZE))))
					.addContainerGap(96, Short.MAX_VALUE))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(57)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING, false)
								.addComponent(btn2, Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 113, GroupLayout.PREFERRED_SIZE)
								.addComponent(btn1, Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 113, GroupLayout.PREFERRED_SIZE))
							.addGap(7))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(btn3, GroupLayout.PREFERRED_SIZE, 113, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(btn4, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE)
						.addComponent(btn5, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE)
						.addComponent(btn6, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(btn8, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE)
						.addComponent(btn7, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE)
						.addComponent(btn9, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(57, Short.MAX_VALUE))
		);
		contentPane.setLayout(gl_contentPane);
		
		btn1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JButton boton = (JButton) e.getSource();
				
				if(jugador == 0) {
					jugador = JUGADOR_UNO;
				}
				
				if(jugador == JUGADOR_UNO) {
					boton.setText("X");
					xo[0] = JUGADOR_UNO;
					jugador = JUGADOR_DOS;
				} else if(jugador == JUGADOR_DOS) {
					boton.setText("O");
					xo[0] = JUGADOR_DOS;
					jugador = JUGADOR_UNO;
				}
				
				if (comprobarGanador() == JUGADOR_UNO) {
					JOptionPane.showMessageDialog(null, "Jugador 1 ha ganado!");
					reiniciarJuego();
				} else if (comprobarGanador() == JUGADOR_DOS) {
					JOptionPane.showMessageDialog(null, "Jugador 2 ha ganado!");
					reiniciarJuego();
				} else if (comprobarGanador() == -1) {
					JOptionPane.showMessageDialog(null, "No hay ganador!");
					reiniciarJuego();
				}
			}
		});
		
		btn2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JButton boton = (JButton) e.getSource();
				
				if(jugador == 0) {
					jugador = JUGADOR_UNO;
				}
				
				if(jugador == JUGADOR_UNO) {
					boton.setText("X");
					xo[1] = JUGADOR_UNO;
					jugador = JUGADOR_DOS;
				} else if(jugador == JUGADOR_DOS) {
					boton.setText("O");
					xo[1] = JUGADOR_DOS;
					jugador = JUGADOR_UNO;
				}
				
				if (comprobarGanador() == JUGADOR_UNO) {
					JOptionPane.showMessageDialog(null, "Jugador 1 ha ganado!");
					reiniciarJuego();
				} else if (comprobarGanador() == JUGADOR_DOS) {
					JOptionPane.showMessageDialog(null, "Jugador 2 ha ganado!");
					reiniciarJuego();
				} else if (comprobarGanador() == -1) {
					JOptionPane.showMessageDialog(null, "No hay ganador!");
					reiniciarJuego();
				}
				
			}
		});
		
		btn3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JButton boton = (JButton) e.getSource();
				
				if(jugador == 0) {
					jugador = JUGADOR_UNO;
				}
				
				if(jugador == JUGADOR_UNO) {
					boton.setText("X");
					xo[2] = JUGADOR_UNO;
					jugador = JUGADOR_DOS;
				} else if(jugador == JUGADOR_DOS) {
					boton.setText("O");
					xo[2] = JUGADOR_DOS;
					jugador = JUGADOR_UNO;
				}
				
				if (comprobarGanador() == JUGADOR_UNO) {
					JOptionPane.showMessageDialog(null, "Jugador 1 ha ganado!");
					reiniciarJuego();
				} else if (comprobarGanador() == JUGADOR_DOS) {
					JOptionPane.showMessageDialog(null, "Jugador 2 ha ganado!");
					reiniciarJuego();
				} else if (comprobarGanador() == -1) {
					JOptionPane.showMessageDialog(null, "No hay ganador!");
					reiniciarJuego();
				}
			}
		});
		
		btn4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JButton boton = (JButton) e.getSource();
				
				if(jugador == 0) {
					jugador = JUGADOR_UNO;
				}
				
				if(jugador == JUGADOR_UNO) {
					boton.setText("X");
					xo[3] = JUGADOR_UNO;
					jugador = JUGADOR_DOS;
				} else if(jugador == JUGADOR_DOS) {
					boton.setText("O");
					xo[3] = JUGADOR_DOS;
					jugador = JUGADOR_UNO;
				}
				
				if (comprobarGanador() == JUGADOR_UNO) {
					JOptionPane.showMessageDialog(null, "Jugador 1 ha ganado!");
					reiniciarJuego();
				} else if (comprobarGanador() == JUGADOR_DOS) {
					JOptionPane.showMessageDialog(null, "Jugador 2 ha ganado!");
					reiniciarJuego();
				} else if (comprobarGanador() == -1) {
					JOptionPane.showMessageDialog(null, "No hay ganador!");
					reiniciarJuego();
				}
			}
		});
		
		btn5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JButton boton = (JButton) e.getSource();
				
				if(jugador == 0) {
					jugador = JUGADOR_UNO;
				}
				
				if(jugador == JUGADOR_UNO) {
					boton.setText("X");
					xo[4] = JUGADOR_UNO;
					jugador = JUGADOR_DOS;
				} else if(jugador == JUGADOR_DOS) {
					boton.setText("O");
					xo[4] = JUGADOR_DOS;
					jugador = JUGADOR_UNO;
				}
				
				if (comprobarGanador() == JUGADOR_UNO) {
					JOptionPane.showMessageDialog(null, "Jugador 1 ha ganado!");
					reiniciarJuego();
				} else if (comprobarGanador() == JUGADOR_DOS) {
					JOptionPane.showMessageDialog(null, "Jugador 2 ha ganado!");
					reiniciarJuego();
				} else if (comprobarGanador() == -1) {
					JOptionPane.showMessageDialog(null, "No hay ganador!");
					reiniciarJuego();
				}
			}
		});
		
		btn6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JButton boton = (JButton) e.getSource();
				
				if(jugador == 0) {
					jugador = JUGADOR_UNO;
				}
				
				if(jugador == JUGADOR_UNO) {
					boton.setText("X");
					xo[5] = JUGADOR_UNO;
					jugador = JUGADOR_DOS;
				} else if(jugador == JUGADOR_DOS) {
					boton.setText("O");
					xo[5] = JUGADOR_DOS;
					jugador = JUGADOR_UNO;
				}
				
				if (comprobarGanador() == JUGADOR_UNO) {
					JOptionPane.showMessageDialog(null, "Jugador 1 ha ganado!");
					reiniciarJuego();
				} else if (comprobarGanador() == JUGADOR_DOS) {
					JOptionPane.showMessageDialog(null, "Jugador 2 ha ganado!");
					reiniciarJuego();
				} else if (comprobarGanador() == -1) {
					JOptionPane.showMessageDialog(null, "No hay ganador!");
					reiniciarJuego();
				}
			}
		});
		
		btn7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JButton boton = (JButton) e.getSource();
				
				if(jugador == 0) {
					jugador = JUGADOR_UNO;
				}
				
				if(jugador == JUGADOR_UNO) {
					boton.setText("X");
					xo[6] = JUGADOR_UNO;
					jugador = JUGADOR_DOS;
				} else if(jugador == JUGADOR_DOS) {
					boton.setText("O");
					xo[6] = JUGADOR_DOS;
					jugador = JUGADOR_UNO;
				}
				
				if (comprobarGanador() == JUGADOR_UNO) {
					JOptionPane.showMessageDialog(null, "Jugador 1 ha ganado!");
					reiniciarJuego();
				} else if (comprobarGanador() == JUGADOR_DOS) {
					JOptionPane.showMessageDialog(null, "Jugador 2 ha ganado!");
					reiniciarJuego();
				} else if (comprobarGanador() == -1) {
					JOptionPane.showMessageDialog(null, "No hay ganador!");
					reiniciarJuego();
				}
			}
		});
		
		btn8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JButton boton = (JButton) e.getSource();
				
				if(jugador == 0) {
					jugador = JUGADOR_UNO;
				}
				
				if(jugador == JUGADOR_UNO) {
					boton.setText("X");
					xo[7] = JUGADOR_UNO;
					jugador = JUGADOR_DOS;
				} else if(jugador == JUGADOR_DOS) {
					boton.setText("O");
					xo[7] = JUGADOR_DOS;
					jugador = JUGADOR_UNO;
				}
				
				if (comprobarGanador() == JUGADOR_UNO) {
					JOptionPane.showMessageDialog(null, "Jugador 1 ha ganado!");
					reiniciarJuego();
				} else if (comprobarGanador() == JUGADOR_DOS) {
					JOptionPane.showMessageDialog(null, "Jugador 2 ha ganado!");
					reiniciarJuego();
				} else if (comprobarGanador() == -1) {
					JOptionPane.showMessageDialog(null, "No hay ganador!");
					reiniciarJuego();
				}
				
			}
		});
		
		btn9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JButton boton = (JButton) e.getSource();
				
				if(jugador == 0) {
					jugador = JUGADOR_UNO;
				}
				
				if(jugador == JUGADOR_UNO) {
					boton.setText("X");
					xo[8] = JUGADOR_UNO;
					jugador = JUGADOR_DOS;
				} else if(jugador == JUGADOR_DOS) {
					boton.setText("O");
					xo[8] = JUGADOR_DOS;
					jugador = JUGADOR_UNO;
				}
				
				if (comprobarGanador() == JUGADOR_UNO) {
					JOptionPane.showMessageDialog(null, "Jugador 1 ha ganado!");
					reiniciarJuego();
				} else if (comprobarGanador() == JUGADOR_DOS) {
					JOptionPane.showMessageDialog(null, "Jugador 2 ha ganado!");
					reiniciarJuego();
				} else if (comprobarGanador() == -1) {
					JOptionPane.showMessageDialog(null, "No hay ganador!");
					reiniciarJuego();
				}
			}
		});
	}
	
	private int comprobarGanador() {
		int ganador = 0;
		
		if (xo[0] == JUGADOR_UNO && xo[1] == JUGADOR_UNO && xo[2] == JUGADOR_UNO) {
			ganador = JUGADOR_UNO;
		} else if (xo[3] == JUGADOR_UNO && xo[4] == JUGADOR_UNO && xo[5] == JUGADOR_UNO) {
			ganador = JUGADOR_UNO;
		} else if (xo[6] == JUGADOR_UNO && xo[7] == JUGADOR_UNO && xo[8] == JUGADOR_UNO) {
			ganador = JUGADOR_UNO;
		} else if (xo[0] == JUGADOR_UNO && xo[3] == JUGADOR_UNO && xo[6] == JUGADOR_UNO) {
			ganador = JUGADOR_UNO;
		} else if (xo[1] == JUGADOR_UNO && xo[4] == JUGADOR_UNO && xo[7] == JUGADOR_UNO) {
			ganador = JUGADOR_UNO;
		} else if (xo[2] == JUGADOR_UNO && xo[5] == JUGADOR_UNO && xo[8] == JUGADOR_UNO) {
			ganador = JUGADOR_UNO;
		} else if (xo[0] == JUGADOR_UNO && xo[4] == JUGADOR_UNO && xo[8] == JUGADOR_UNO) {
			ganador = JUGADOR_UNO;
		} else if (xo[2] == JUGADOR_UNO && xo[4] == JUGADOR_UNO && xo[6] == JUGADOR_UNO) {
			ganador = JUGADOR_UNO;
		}
		
		if (xo[0] == JUGADOR_DOS && xo[1] == JUGADOR_DOS && xo[2] == JUGADOR_DOS) {
			ganador = JUGADOR_DOS;
		} else if (xo[3] == JUGADOR_DOS && xo[4] == JUGADOR_DOS && xo[5] == JUGADOR_DOS) {
			ganador = JUGADOR_DOS;
		} else if (xo[6] == JUGADOR_DOS && xo[7] == JUGADOR_DOS && xo[8] == JUGADOR_DOS) {
			ganador = JUGADOR_DOS;
		} else if (xo[0] == JUGADOR_DOS && xo[3] == JUGADOR_DOS && xo[6] == JUGADOR_DOS) {
			ganador = JUGADOR_DOS;
		} else if (xo[1] == JUGADOR_DOS && xo[4] == JUGADOR_DOS && xo[7] == JUGADOR_DOS) {
			ganador = JUGADOR_DOS;
		} else if (xo[2] == JUGADOR_DOS && xo[5] == JUGADOR_DOS && xo[8] == JUGADOR_DOS) {
			ganador = JUGADOR_DOS;
		} else if (xo[0] == JUGADOR_DOS && xo[4] == JUGADOR_DOS && xo[8] == JUGADOR_DOS) {
			ganador = JUGADOR_DOS;
		} else if (xo[2] == JUGADOR_DOS && xo[4] == JUGADOR_DOS && xo[6] == JUGADOR_DOS) {
			ganador = JUGADOR_DOS;
		}
		
		boolean estaLleno = true;
		for (int i : xo) {
			if (i == 0) {
				estaLleno = false;
				break;
			}
		}
		
		if (estaLleno && ganador == 0) {
			ganador = -1;
		}
		return ganador;
	}
	
	public void reiniciarJuego() {
		btn1.setText("");
		btn2.setText("");
		btn3.setText("");
		btn4.setText("");
		btn5.setText("");
		btn6.setText("");
		btn7.setText("");
		btn8.setText("");
		btn9.setText("");
		
		for (int i = 0; i < xo.length; i++) {
			xo[i] = 0;
		}
		
		jugador = 0;
	}
}
